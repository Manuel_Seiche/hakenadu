import {SingleLineComponent} from "./single-line.component";
import {NgModule} from "@angular/core";

@NgModule({
  declarations: [
    SingleLineComponent
  ],
  exports: [
    SingleLineComponent
  ]
})
export class CommonModule {}
