import {Component} from "@angular/core";

@Component({
  selector: 'single-line',
  template: '<div class="bg-light row justify-content-center"><div class="p-0 col-12"><ng-content></ng-content></div></div>'
})
export class SingleLineComponent {}
