import {BrowserModule} from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPageScrollModule} from 'ngx-page-scroll';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {AiComponent} from './ai/ai.component';
import {UiComponent} from './ui/ui.component';
import {WsComponent} from './ws/ws.component';
import {AppRoutingModule} from './app-routing.module';
import {RouterModule} from "@angular/router";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {VaadinComponent} from './ui/vaadin/vaadin.component';
import {CommonModule} from "../common/common.module";
import {X3dComponent} from './x3d/x3d.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AiComponent,
    UiComponent,
    WsComponent,
    VaadinComponent,
    X3dComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    NgbModule,
    NgxPageScrollModule,
    AppRoutingModule,
    RouterModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
