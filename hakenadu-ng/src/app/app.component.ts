import {Component, ElementRef, EventEmitter, HostListener, Inject, ViewChild} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {PageScrollConfig, PageScrollInstance, PageScrollService} from 'ngx-page-scroll';
import {NavigationEnd, Router} from "@angular/router";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [trigger('slideNavInOut', [
    state('in', style({transform: 'translate3d(0,0,0)'})),
    state('out', style({transform: 'translate3d(-120%,0%,0)'})),
    transition('in => out', animate('400ms ease-in-out')),
    transition('out => in', animate('400ms ease-in-out'))
  ])]
})
export class AppComponent {

  @ViewChild('stickynav')
  private stickyNav: ElementRef;

  private menuState = 'out';

  title = 'hakenadu-ng';
  isNavbarCollapsed = true;

  constructor(private router: Router, private pageScrollService: PageScrollService, @Inject(DOCUMENT) private document: any) {
    router.events.subscribe(this.routerEventFired.bind(this));
    PageScrollConfig.defaultDuration = 666;
    PageScrollConfig.defaultEasingLogic = {
      ease: (t: number, b: number, c: number, d: number): number => {
        // easeInOutExpo easing
        if (t === 0) return b;
        if (t === d) return b + c;
        if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
      }
    };
  }

  private routerEventFired(event: Event): void {
    if (event instanceof NavigationEnd) {
      let navigationEnd: NavigationEnd = <NavigationEnd>event;
      let rawURL = navigationEnd.urlAfterRedirects;
      if (rawURL && rawURL !== null && rawURL.trim().length > 0) {
        let path = rawURL.substring(1);
        if (this.isValidRouterPath(path)) {
          console.log('navigating to ' + path);
          this.scroll('#' + path);
        }
      }
    }
  }

  public isValidRouterPath(path: string): boolean {
    for (let confIndex = 0; confIndex < this.router.config.length; confIndex++) {
      if (this.router.config[confIndex].path === path) {
        return true;
      }
    }
    return false;
  }

  ngAfterViewInit() {
    this.updateScrollOffset();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.updateScrollOffset();
  }

  private updateScrollOffset(): void {
    PageScrollConfig.defaultScrollOffset = this.stickyNav.nativeElement.offsetHeight;
  }

  public toggleAnimation(): void {
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
    console.log(this.menuState);
  }

  public slideNavInOutDone(event: Event): void {
    this.toggleNavbar();
  }

  public toggleNavbar(): void {
    if (this.isNavbarCollapsed) {
      this.showNavbar();
    } else {
      this.hideNavbar();
    }
  }

  public hideNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  public showNavbar(): void {
    this.isNavbarCollapsed = false;
  }

  private scroll(target: string): void {
    this.pageScrollService.start(this.createPageScrollInstance(target));
  }

  private scrollFinished(reachedTarget: boolean) {
    if (reachedTarget) {
      this.hideNavbar();
    }
  }

  private createPageScrollInstance(target: string): PageScrollInstance {
    let emitter = new EventEmitter<boolean>();
    emitter.subscribe(this.scrollFinished.bind(this));
    return PageScrollInstance.newInstance({
      document: this.document,
      scrollTarget: target,
      pageScrollFinishListener: emitter
    });
  }
}
